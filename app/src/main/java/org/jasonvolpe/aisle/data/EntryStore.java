package org.jasonvolpe.aisle.data;

import android.util.JsonWriter;
import android.util.Log;

import org.jasonvolpe.aisle.common.utils.FileUtils;
import org.jasonvolpe.aisle.model.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Manages and persists shopping list entries.
 */
public class EntryStore {
    private static final String TAG = EntryStore.class.getSimpleName();

    private static final String DEFAULT_FILENAME = "entries.json";

    public interface EntryStoreListener {
        void onChange();
    }

    private static List<Entry> sEntries;
    private static EntryStoreListener sListener;

    private File mJsonFile;

    private Filter mFilter = Filter.UNPURCHASED;

    public enum Filter {
        UNPURCHASED,
        PURCHASED
    }

    public EntryStore(File dataDirectory) {
        mJsonFile = new File(dataDirectory, DEFAULT_FILENAME);

        if (sEntries == null) {
            sEntries = new ArrayList<Entry>();
        }
        sEntries.clear();
        sEntries.addAll(getEntriesFromJsonFile());
    }

    private List<Entry> getEntriesFromJsonFile() {
        makeFileIfNone();

        List<Entry> entries = new ArrayList<Entry>();

        try {
            String jsonString = FileUtils.stringFromFile(mJsonFile);
            Log.d(TAG, jsonString);
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonEntries = jsonObject.getJSONArray("entries");
            JSONObject jsonEntry;
            for (int i = 0; i < jsonEntries.length(); i++) {
                jsonEntry = (JSONObject) jsonEntries.get(i);
                Entry entry = new Entry(jsonEntry.getString("product"), jsonEntry.getString("memo"), jsonEntry.getDouble("price"), jsonEntry.getBoolean("is_purchased"));
                entries.add(entry);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entries;
    }

    private void makeFileIfNone() {
        if (!mJsonFile.exists()) {
            try {
                createEmptyJsonFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createEmptyJsonFile() throws IOException {
        JsonWriter writer = new JsonWriter(new FileWriter(mJsonFile));
        writer.beginObject();
        writer.name("version").value(1);
        writer.name("entries").beginArray()
                .endArray();
        writer.endObject();
        writer.close();
    }

    public void setListener(EntryStoreListener listener) {
        sListener = listener;
    }

    public void add(Entry entry) {
        sEntries.add(entry);

        persistEntries();

        notifyListeners();
    }

    private void persistEntries() {
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new FileWriter(mJsonFile));
            writer.beginObject();

            writer.name("version").value(1);

            writer.name("entries").beginArray();
            for (Entry entry : sEntries) {
                writer.beginObject();
                writer.name("product").value(entry.getProduct());
                writer.name("memo").value(entry.getMemo());
                writer.name("price").value(entry.getPrice());
                writer.name("is_purchased").value(entry.isPurchased());
                writer.endObject();
            }
            writer.endArray();

            writer.endObject();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void notifyListeners() {
        if (sListener != null) {
            sListener.onChange();
        }
    }

    public Entry get(int index) {
        return getFilteredEntries().get(index);
    }

    private List<Entry> getFilteredEntries() {
        List<Entry> filteredEntries = new ArrayList<Entry>();

        for (Entry entry : sEntries) {
            switch (mFilter) {
                case UNPURCHASED:
                    if (!entry.isPurchased()) {
                        filteredEntries.add(entry);
                    }
                    break;
                case PURCHASED:
                    if (entry.isPurchased()) {
                        filteredEntries.add(entry);
                    }
                    break;
            }
        }
        return filteredEntries;
    }

    public int count() {
        return getFilteredEntries().size();
    }

    public double getTotalPrice() {
        double total = 0;
        for (Entry entry : getFilteredEntries()) {
            total += entry.getPrice();
        }
        return total;
    }

    public void clear() {
        sEntries.clear();
        persistEntries();
        notifyListeners();
    }

    public void setFilter(Filter filter) {
        if (mFilter != filter) {
            mFilter = filter;
            notifyListeners();
        }
    }

    // Todo: this should be replaced with a "persistable" Entry wrapper or something else
    public void setIsPurchased(Entry entry, boolean isPurchased) {
        for (Entry e : sEntries) {
            if (e.equals(entry)) {
                e.setIsPurchased(isPurchased);
                persistEntries();
            }
        }
    }
}
