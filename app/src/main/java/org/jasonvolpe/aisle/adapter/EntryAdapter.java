package org.jasonvolpe.aisle.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.jasonvolpe.aisle.R;
import org.jasonvolpe.aisle.data.EntryStore;
import org.jasonvolpe.aisle.ui.MainActivity;

/**
 * @author jvo
 */
public class EntryAdapter extends RecyclerView.Adapter<EntryViewHolder> {

    private MainActivity mMainActivity;
    private EntryStore mEntryStore;

    public EntryAdapter(MainActivity mainActivity, EntryStore entryStore) {
        mMainActivity = mainActivity;
        mEntryStore = entryStore;
    }

    @Override
    public EntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(mMainActivity.getApplicationContext(), R.layout.item_entry, null);
        return new EntryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EntryViewHolder holder, int position) {
        holder.bind(mEntryStore.get(position), mEntryStore);
    }

    @Override
    public int getItemCount() {
        return mEntryStore.count();
    }
}
