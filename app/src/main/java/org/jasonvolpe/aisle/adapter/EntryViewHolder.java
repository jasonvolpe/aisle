package org.jasonvolpe.aisle.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.jasonvolpe.aisle.R;
import org.jasonvolpe.aisle.data.EntryStore;
import org.jasonvolpe.aisle.model.Entry;

/**
 * @author jvo
 */
public class EntryViewHolder extends RecyclerView.ViewHolder {
    TextView mTitleView;
    TextView mMemoView;
    TextView mPriceView;
    CheckBox mIsPurchasedCheck;

    public EntryViewHolder(View itemView) {
        super(itemView);

        mTitleView = (TextView) itemView.findViewById(R.id.entry_title);
        mMemoView = (TextView) itemView.findViewById(R.id.entry_memo);
        mPriceView = (TextView) itemView.findViewById(R.id.entry_price);
        mIsPurchasedCheck = (CheckBox) itemView.findViewById(R.id.purchased_checkbox);
    }

    public void bind(final Entry entry, final EntryStore entryStore) {
        mTitleView.setText(entry.getProduct());

        if (TextUtils.isEmpty(entry.getMemo())) {
            mMemoView.setVisibility(View.GONE);
            mMemoView.setText("");
        } else {
            mMemoView.setText(entry.getMemo());
            mMemoView.setVisibility(View.VISIBLE);
        }

        mPriceView.setText("$" + entry.getPrice());

        mIsPurchasedCheck.setChecked(entry.isPurchased());

        mIsPurchasedCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                entryStore.setIsPurchased(entry, isChecked);
            }
        });
    }
}
