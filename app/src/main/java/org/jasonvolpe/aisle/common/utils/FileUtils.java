package org.jasonvolpe.aisle.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author jvo
 */
public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();

    public static String stringFromFile(File file) throws IOException {
        StringBuffer sb = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            sb.append(line);
        }
        return sb.toString();
    }
}
