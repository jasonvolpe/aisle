package org.jasonvolpe.aisle.model;

/**
 * @author jgv
 */
public class Entry {
    private static final String TAG = Entry.class.getName();

    private String mProduct;
    private String mMemo;
    private double mPrice = 0;
    private boolean mIsPurchased;

    public Entry(String product, String memo) {
        mProduct = product;
        mMemo = memo;
    }

    public Entry(String product, String memo, double price) {
        mProduct = product;
        mMemo = memo;
        mPrice = price;
    }

    public Entry(String product, String memo, double price, boolean isPurchased) {
        mProduct = product;
        mMemo = memo;
        mPrice = price;
        mIsPurchased = isPurchased;
    }

    public String getProduct() {
        return mProduct;
    }

    public String getMemo() {
        return mMemo;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public boolean isPurchased() {
        return mIsPurchased;
    }

    public void setIsPurchased(boolean isPurchased) {
        mIsPurchased = isPurchased;
    }

}
