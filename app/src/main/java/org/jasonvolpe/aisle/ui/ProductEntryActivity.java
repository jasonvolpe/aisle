package org.jasonvolpe.aisle.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.jasonvolpe.aisle.R;
import org.jasonvolpe.aisle.common.networking.VolleyQueue;
import org.jasonvolpe.aisle.data.EntryStore;
import org.jasonvolpe.aisle.model.Entry;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ProductEntryActivity extends AppCompatActivity {
    private static final String TAG = ProductEntryActivity.class.getSimpleName();

    public static final int NEW_ENTRY_REQUEST_CODE = 7;

    private EntryStore mEntryStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_entry);

        mEntryStore = new EntryStore(getApplicationContext().getFilesDir());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_entry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.done) {
            if (TextUtils.isEmpty(getProduct())) {
                Toast.makeText(this, "Missing item...", Toast.LENGTH_SHORT).show();
                return true;
            }

            fetchPriceAndSave();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchPriceAndSave() {
        final Entry entry = new Entry(getProduct(), getMemo());

        String encodedProduct = "";
        try {
            encodedProduct = URLEncoder.encode(entry.getProduct(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, "Error encoding: " + entry.getProduct(), Toast.LENGTH_SHORT).show();
            return;
        }

        final String url = "http://item-price.herokuapp.com/get_price?item=" + encodedProduct;

        JsonObjectRequest priceRequest = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.v(TAG, "#onResponse url: " + url + " response: " + response.toString());

                try {
                    entry.setPrice(response.getDouble("price"));
                    mEntryStore.add(entry);
                    finish();
                } catch (JSONException e) {
                    Log.e(TAG, "Exception parsing price data", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error getting price: " + url, error);
            }
        });

        VolleyQueue.getInstance(getApplicationContext()).addToRequestQueue(priceRequest);

    }

    public String getProduct() {
        return ((EditText) findViewById(R.id.product_text)).getEditableText().toString();
    }

    public String getMemo() {
        return ((EditText) findViewById(R.id.memo_text)).getEditableText().toString();
    }

    public String getTags() {
        return ((EditText) findViewById(R.id.tags_text)).getEditableText().toString();
    }

}
