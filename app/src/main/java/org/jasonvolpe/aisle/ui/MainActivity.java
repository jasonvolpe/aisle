package org.jasonvolpe.aisle.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jasonvolpe.aisle.R;
import org.jasonvolpe.aisle.adapter.EntryAdapter;
import org.jasonvolpe.aisle.adapter.EntryViewHolder;
import org.jasonvolpe.aisle.data.EntryStore;


public class MainActivity extends AppCompatActivity implements EntryStore.EntryStoreListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private EntryStore mEntryStore;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter<EntryViewHolder> mAdapter;

    private boolean showPurchased;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "#onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initEntryStore();

        initUi();
    }

    private void initEntryStore() {
        mEntryStore = new EntryStore(getApplicationContext().getFilesDir());
        mEntryStore.setListener(this);
    }

    private void initUi() {

        initEntriesList();

        updateTotalPrice();

        ((Button) findViewById(R.id.new_entry_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), ProductEntryActivity.class), ProductEntryActivity.NEW_ENTRY_REQUEST_CODE);
            }
        });
    }

    private void initEntriesList() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new EntryAdapter(this, mEntryStore);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.removeAllViews();
    }


    // EntryStore Listener
    @Override
    public void onChange() {
        mAdapter.notifyDataSetChanged();
        updateTotalPrice();
    }

    void updateTotalPrice() {
        TextView totalText = ((TextView) findViewById(R.id.total_text));
        if (totalText != null) {
            totalText.setText("$" + mEntryStore.getTotalPrice());
        }
    }


    @Override
    protected void onResume() {
        Log.v(TAG, "#onResume");
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        toggleFilterMenuText(menu);

        return super.onPrepareOptionsPanel(view, menu);
    }

    private void toggleFilterMenuText(Menu menu) {
        menu.findItem(R.id.action_filter_purchased).setTitle(
                showPurchased ? getString(R.string.action_show_unpurchased) : getString(R.string.action_show_purchased)
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_clear_store){
            mEntryStore.clear();

        } else if (id == R.id.action_filter_purchased) {
            showPurchased = !showPurchased;
            mEntryStore.setFilter(showPurchased ? EntryStore.Filter.PURCHASED : EntryStore.Filter.UNPURCHASED);
        }

        return super.onOptionsItemSelected(item);
    }
}
