# Aisle

## Shopping List App

Aisle > I'll > I will...

## Setup
*  Clone the repo
*  Open the directory as a project in Android Studio
*  Build or run the app

## Features
*  Add items to the shopping list
*  Prices are fetched from web service for items
*  Mark items as purchased and toggle filter in menu

